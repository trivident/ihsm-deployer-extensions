package com.tridion.ihsm.deployer.modules;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.tridion.configuration.Configuration;
import com.tridion.configuration.ConfigurationException;
import com.tridion.deployer.Module;
import com.tridion.deployer.ProcessingException;
import com.tridion.transport.transportpackage.TransportPackage;
import com.tridion.ihsm.deployer.modules.util.XmlUtils;


public class TransformPageUrlModule extends Module {

	protected static Logger logger = LoggerFactory.getLogger(TransformPageUrlModule.class);
	
	// configuration attributes
	private static String CONFIGURATION_ELMT = "Configuration";
	public static String SDLWEB85_VERSION = "SDLWeb85";
	private static String ENABLED_ATTR = "Enabled";
	private static String TRANSFORM_TARGETS_EL = "TransformTargets";

	private boolean enabled = true;
	private List<TransformTarget> targets = new ArrayList<>();
	
	public TransformPageUrlModule(Configuration config) throws ConfigurationException {
		super(config);
		
		enabled = false;

		Configuration c2 = config.getChild(CONFIGURATION_ELMT);
		if (c2 == null) {
			logger.warn("TransformPageUrlModule is incorrectly configured (" + CONFIGURATION_ELMT + " element missing inside Module - translation of URLs is disabled)");
			return;
		}

		if(c2.hasChild(TRANSFORM_TARGETS_EL)){
			Configuration transformTargets = c2.getChild(TRANSFORM_TARGETS_EL);
			for (Configuration target : transformTargets.getChildren()){
				targets.add(new TransformTarget(target));
			}
		}
		else{
			logger.info("No structure groups are configured for TransformPageUrlModule");
			enabled = false;
		}
		
		if (c2.hasAttribute(ENABLED_ATTR)) {
			enabled = Boolean.parseBoolean(c2.getAttribute(ENABLED_ATTR));
			if (!enabled) {
				logger.debug("TransformPageUrlModule is disabled in the configuration");
			}
		}
	}

	@Override
	public void process(TransportPackage transportPackage) throws ProcessingException {
		if (!enabled) {
			return;
		}
		
		logger.debug("called process for transport package " + transportPackage.getTransactionId().toString());

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			String pagesXmlPath = transportPackage.getLocationPath() + File.separator + "pages.xml";
			builder = factory.newDocumentBuilder();

			if(!new File(pagesXmlPath).exists()) {
			    logger.debug("No pages found");
			    return;
            }
            Document doc = builder.parse(pagesXmlPath);
			if (logger.isDebugEnabled()) {
				logger.debug("pages.xml:\r\n" + XmlUtils.getOuterXml(doc));
			}
			
			NodeList pages = XmlUtils.getNodeList(doc, "Pages/Page");

			boolean madeChanges = false;
			for (int i = 0; i < pages.getLength(); i++) {
				Element page = (Element) pages.item(i);
				String pageURL = XmlUtils.getInnerText(page, "URL");

				for(TransformTarget target : targets){
					if(target.Contains(pageURL)){
						logger.debug("URL: " + pageURL + " To be transformed by target " + target.getName());
						pageURL = target.Transform(pageURL);
						logger.debug("TRANSFORMED URL: " + pageURL);

						Element urlElmt = XmlUtils.getElement(page, "URL");
						urlElmt.setTextContent(pageURL);
						madeChanges = true;
					}
				}
			}
			
			if (madeChanges) {
				// save pages.xml
				XmlUtils.saveXmlDocumentToFile(doc, pagesXmlPath);
				logger.debug("updated " + pagesXmlPath);
			} else {
				logger.debug("no changes made");
			}			
		} catch (ParserConfigurationException e) {
			 logger.warn("error while transforming the URLs", e);
		} catch (SAXException e) {
			 logger.warn("error while transforming the URLs", e);
		} catch (IOException e) {
			 logger.warn("error while transforming the URLs", e);
		} catch (XPathExpressionException e) {
			logger.error("error running XPath on pages.xml", e);
		} catch (javax.naming.ConfigurationException e) {
			logger.error("Configuration error in XML", e);
		}
	}

}

package com.tridion.ihsm.deployer.modules.util;

public class FileUtils {
	public static String getFileExtension(String p) {
		int i = p.lastIndexOf('.');
		if (i > 0) {
		    return p.substring(i+1);
		}
		return "";
	}
	public static String removeFirstSegment(String p) {
		String[] s = p.split("/");
		if (s.length == 1) {
			return "";
		}
		String r = "";
		for (int i = 1; i < s.length; i++) {
			if (!r.equals("")) {
				r += "/";
			}
			r += s[i];
		}
		return r;
	}
	public static String removeIdentifier(String p) {
		return p.replaceAll("\\-[0-9]+$", "");
	}
}

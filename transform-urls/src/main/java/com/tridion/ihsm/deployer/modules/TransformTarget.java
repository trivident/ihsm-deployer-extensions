package com.tridion.ihsm.deployer.modules;

import com.tridion.configuration.Configuration;
import com.tridion.configuration.ConfigurationException;

public class TransformTarget {
    private String Name;
    private String SearchPattern;
    private String ReplacePattern;

    TransformTarget(String name, String searchPattern, String replacePattern){
        Name = name;
        SearchPattern = searchPattern;
        ReplacePattern = replacePattern;
    }

    TransformTarget(Configuration configuration) throws ConfigurationException {
       Name = configuration.getAttribute("name", "Unspecified");
       SearchPattern = configuration.getAttribute("searchPattern");
       ReplacePattern = configuration.getAttribute("replacePattern");
    }

    public boolean Contains(String url){
        return url.matches(SearchPattern);
    }

    public String Transform(String url){
        return url.replaceAll(SearchPattern, ReplacePattern);
    }

    public String getName(){
        return Name;
    }

}

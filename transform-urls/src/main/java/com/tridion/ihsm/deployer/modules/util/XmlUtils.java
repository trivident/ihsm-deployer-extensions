package com.tridion.ihsm.deployer.modules.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;

import javax.naming.ConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlUtils {
	protected static Logger logger = LoggerFactory.getLogger(XmlUtils.class);
	
	public static String getOuterXml(Node node) {
		try {
			StreamResult xmlOutput = new StreamResult(new StringWriter());
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.transform(new DOMSource(node), xmlOutput);
			return xmlOutput.getWriter().toString();
		} catch (TransformerException e) {
			logger.error("caught exception while transforming XML", e);
			return "";
		}
	}

	public static NodeList getNodeList(Node node, String xpath) throws XPathExpressionException {
		XPathExpression xpe = getXpathExpression(xpath);
		return (NodeList) xpe.evaluate(node, XPathConstants.NODESET);
	}

	public static Element getElement(Node node, String xpath) throws XPathExpressionException {
		XPathExpression xpe = getXpathExpression(xpath);
		return (Element) xpe.evaluate(node, XPathConstants.NODE);
	}

	public static String getInnerText(Node node, String xpath) throws XPathExpressionException, ConfigurationException {
		XPathExpression xpe = getXpathExpression(xpath);
		Element elmt = (Element) xpe.evaluate(node, XPathConstants.NODE);
		if (elmt == null) {
			throw new ConfigurationException();
		}
		return elmt.getTextContent();
	}
	
	public static void saveXmlDocumentToFile(Document doc, String filename) {
		 try {
	            Transformer tr = TransformerFactory.newInstance().newTransformer();
	            tr.setOutputProperty(OutputKeys.INDENT, "yes");
	            tr.setOutputProperty(OutputKeys.METHOD, "xml");
	            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

			 	FileOutputStream fileOutputStream = new FileOutputStream(filename);
	            // send DOM to file
	            tr.transform(new DOMSource(doc), 
	                                 new StreamResult(fileOutputStream));

	            fileOutputStream.close();

	        } catch (TransformerException e) {
	            logger.warn("error saving xml to file " + filename, e);
	        } catch (IOException e) {
	            logger.warn("error saving xml to file " + filename, e);
			}
	}
	
	private static XPathExpression getXpathExpression(String xpath) throws XPathExpressionException {
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpathObj = xPathfactory.newXPath();
		return xpathObj.compile(xpath);
	}
}
